package com.example.weather_predictor

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class Predictor : AppCompatActivity() {
    var btnProses: Button? = null
    var tvOutput: TextView? = null

    var chkGejala1: CheckBox? = null
    var chkGejala2: CheckBox? = null
    var chkGejala3: CheckBox? = null
    var chkGejala4: CheckBox? = null

    var txtNilaiGejala1: AutoCompleteTextView? = null
    var txtNilaiGejala2: AutoCompleteTextView? = null
    var txtNilaiGejala3: AutoCompleteTextView? = null
    var txtNilaiGejala4: AutoCompleteTextView? = null

    var nilaiKeyakinanGejala1 = arrayOf("", "0","0.1", "0.2","0.3", "0.4","0.5", "0.6","0.7", "0.8","0.9", "1")
    var nilaiKeyakinanGejala2 = arrayOf("", "0","0.1", "0.2","0.3", "0.4","0.5", "0.6","0.7", "0.8","0.9", "1")
    var nilaiKeyakinanGejala3 = arrayOf("", "0","0.1", "0.2","0.3", "0.4","0.5", "0.6","0.7", "0.8","0.9", "1")
    var nilaiKeyakinanGejala4 = arrayOf("", "0","0.1", "0.2","0.3", "0.4","0.5", "0.6","0.7", "0.8","0.9", "1")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_predictor)
        btnProses = findViewById<View>(R.id.button) as Button
        tvOutput = findViewById<View>(R.id.textView) as TextView

        chkGejala1 = findViewById<View>(R.id.checkBox) as CheckBox
        chkGejala2 = findViewById<View>(R.id.checkBox2) as CheckBox
        chkGejala3 = findViewById<View>(R.id.checkBox3) as CheckBox
        chkGejala4 = findViewById<View>(R.id.checkBox4) as CheckBox

        txtNilaiGejala1 = findViewById<View>(R.id.autoCompleteTextView) as AutoCompleteTextView
        txtNilaiGejala2 = findViewById<View>(R.id.autoCompleteTextView2) as AutoCompleteTextView
        txtNilaiGejala3 = findViewById<View>(R.id.autoCompleteTextView3) as AutoCompleteTextView
        txtNilaiGejala4 = findViewById<View>(R.id.autoCompleteTextView4) as AutoCompleteTextView

        val adapterGejala1 = ArrayAdapter(this, android.R.layout.select_dialog_item, nilaiKeyakinanGejala1)
        txtNilaiGejala1!!.threshold = 1
        txtNilaiGejala1!!.setAdapter(adapterGejala1)
        val adapterGejala2 = ArrayAdapter(this, android.R.layout.select_dialog_item, nilaiKeyakinanGejala2)
        txtNilaiGejala2!!.threshold = 1
        txtNilaiGejala2!!.setAdapter(adapterGejala2)
        val adapterGejala3 = ArrayAdapter(this, android.R.layout.select_dialog_item, nilaiKeyakinanGejala3)
        txtNilaiGejala3!!.threshold = 1
        txtNilaiGejala3!!.setAdapter(adapterGejala3)
        val adapterGejala4 = ArrayAdapter(this, android.R.layout.select_dialog_item, nilaiKeyakinanGejala4)
        txtNilaiGejala4!!.threshold = 1
        txtNilaiGejala4!!.setAdapter(adapterGejala4)

        txtNilaiGejala1!!.setOnClickListener {
            AlertDialog.Builder(this@Predictor).setTitle("Pilihlah Nilai Kelembaban").setAdapter(adapterGejala1) { dialog, which ->
                txtNilaiGejala1!!.setText(nilaiKeyakinanGejala1[which])
                dialog.dismiss()
            }.create().show()
        }
        txtNilaiGejala2!!.setOnClickListener {
            AlertDialog.Builder(this@Predictor).setTitle("Pilihlah Nilai Kecepatan Angin Vertikal").setAdapter(adapterGejala2) { dialog, which ->
                txtNilaiGejala2!!.setText(nilaiKeyakinanGejala2[which])
                dialog.dismiss()
            }.create().show()
        }
        txtNilaiGejala3!!.setOnClickListener {
            AlertDialog.Builder(this@Predictor).setTitle("Pilihlah Nilai Suhu Udara").setAdapter(adapterGejala3) { dialog, which ->
                txtNilaiGejala3!!.setText(nilaiKeyakinanGejala3[which])
                dialog.dismiss()
            }.create().show()
        }
        txtNilaiGejala4!!.setOnClickListener {
            AlertDialog.Builder(this@Predictor).setTitle("Pilihlah Nilai Kuantitas Awan").setAdapter(adapterGejala4) { dialog, which ->
                txtNilaiGejala4!!.setText(nilaiKeyakinanGejala4[which])
                dialog.dismiss()
            }.create().show()
        }

        btnProses!!.setOnClickListener {
            var PrediksiCuaca = " Presentase Perakiraan Cuaca :"


            // AND && dan OR ||
            if (chkGejala1!!.isChecked && chkGejala2!!.isChecked && chkGejala3!!.isChecked) {

            //  Nilai dari PAKAR / AHLINNYA
            val nilaiGejala1 = 0.4
            val nilaiGejala2 = 0.3
            val nilaiGejala3 = 0.4


            // Nilai Inputan dari  USER
            val doubleGejala1 = txtNilaiGejala1!!.text.toString().toDouble()
            val doubleGejala2 = txtNilaiGejala2!!.text.toString().toDouble()
            val doubleGejala3 = txtNilaiGejala3!!.text.toString().toDouble()

            val hasilHitunganGejala1 = nilaiGejala1 * doubleGejala1
            val hasilHitunganGejala2 = nilaiGejala2 * doubleGejala2
            val hasilHitunganGejala3 = nilaiGejala3 * doubleGejala3


            val Combine_CF1_CF2 = hasilHitunganGejala1 + hasilHitunganGejala2 * (1 - hasilHitunganGejala1)
            val Combine_CFold_CF3 = Combine_CF1_CF2 + hasilHitunganGejala3 * (1 - Combine_CF1_CF2)
            val hasilHitungGejalaCuacaA = (Combine_CFold_CF3 * 100).toString()
            PrediksiCuaca += "\nTidak Hujan\n$hasilHitungGejalaCuacaA %"
        }

            if (chkGejala1!!.isChecked && chkGejala2!!.isChecked && chkGejala3!!.isChecked && chkGejala4!!.isChecked) {

                //  Nilai dari PAKAR / AHLINNYA
                val nilaiGejala1 = 0.3
                val nilaiGejala2 = 0.3
                val nilaiGejala3 = 0.3
                val nilaiGejala4 = 0.3

                // Nilai Inputan dari  USER
                val doubleGejala1 = txtNilaiGejala1!!.text.toString().toDouble()
                val doubleGejala2 = txtNilaiGejala2!!.text.toString().toDouble()
                val doubleGejala3 = txtNilaiGejala3!!.text.toString().toDouble()
                val doubleGejala4 = txtNilaiGejala4!!.text.toString().toDouble()

                val hasilHitunganGejala1 = nilaiGejala1 * doubleGejala1
                val hasilHitunganGejala2 = nilaiGejala2 * doubleGejala2
                val hasilHitunganGejala3 = nilaiGejala3 * doubleGejala3
                val hasilHitunganGejala4 = nilaiGejala4 * doubleGejala4

                val Combine_CF1_CF2 = hasilHitunganGejala1 + hasilHitunganGejala2 * (1 - hasilHitunganGejala1)
                val Combine_CFold_CF3 = Combine_CF1_CF2 + hasilHitunganGejala3 * (1 - Combine_CF1_CF2)
                val Combine_CFold_CF4 = Combine_CFold_CF3 + hasilHitunganGejala4 * (1 - Combine_CFold_CF3)
                val hasilHitungGejalaCuacaA = (Combine_CFold_CF4 * 100).toString()
                PrediksiCuaca += "\nGerimis\n$hasilHitungGejalaCuacaA %"
            }
            if (chkGejala1!!.isChecked && chkGejala2!!.isChecked && chkGejala3!!.isChecked && chkGejala4!!.isChecked) {

                //  Nilai dari PAKAR / AHLINNYA
                val nilaiGejala1 = 0.6
                val nilaiGejala2 = 0.6
                val nilaiGejala3 = 0.5
                val nilaiGejala4 = 0.5

                // Nilai Inputan dari  USER
                val doubleGejala1 = txtNilaiGejala1!!.text.toString().toDouble()
                val doubleGejala2 = txtNilaiGejala2!!.text.toString().toDouble()
                val doubleGejala3 = txtNilaiGejala3!!.text.toString().toDouble()
                val doubleGejala4 = txtNilaiGejala4!!.text.toString().toDouble()

                val hasilHitunganGejala1 = nilaiGejala1 * doubleGejala1
                val hasilHitunganGejala2 = nilaiGejala2 * doubleGejala2
                val hasilHitunganGejala3 = nilaiGejala3 * doubleGejala3
                val hasilHitunganGejala4 = nilaiGejala4 * doubleGejala4

                val Combine_CF1_CF2 = hasilHitunganGejala1 + hasilHitunganGejala2 * (1 - hasilHitunganGejala1)
                val Combine_CFold_CF3 = Combine_CF1_CF2 + hasilHitunganGejala3 * (1 - Combine_CF1_CF2)
                val Combine_CFold_CF4 = Combine_CFold_CF3 + hasilHitunganGejala4 * (1 - Combine_CFold_CF3)
                val hasilHitungGejalaCuacaA = (Combine_CFold_CF4 * 100).toString()
                PrediksiCuaca += "\nHujan Sedang \n$hasilHitungGejalaCuacaA %"
            }
            if (chkGejala1!!.isChecked && chkGejala2!!.isChecked && chkGejala3!!.isChecked && chkGejala4!!.isChecked) {

                //  Nilai dari PAKAR / AHLINNYA
                val nilaiGejala1 = 0.7
                val nilaiGejala2 = 0.7
                val nilaiGejala3 = 0.7
                val nilaiGejala4 = 0.7

                // Nilai Inputan dari  USER
                val doubleGejala1 = txtNilaiGejala1!!.text.toString().toDouble()
                val doubleGejala2 = txtNilaiGejala2!!.text.toString().toDouble()
                val doubleGejala3 = txtNilaiGejala3!!.text.toString().toDouble()
                val doubleGejala4 = txtNilaiGejala4!!.text.toString().toDouble()

                val hasilHitunganGejala1 = nilaiGejala1 * doubleGejala1
                val hasilHitunganGejala2 = nilaiGejala2 * doubleGejala2
                val hasilHitunganGejala3 = nilaiGejala3 * doubleGejala3
                val hasilHitunganGejala4 = nilaiGejala4 * doubleGejala4

                val Combine_CF1_CF2 = hasilHitunganGejala1 + hasilHitunganGejala2 * (1 - hasilHitunganGejala1)
                val Combine_CFold_CF3 = Combine_CF1_CF2 + hasilHitunganGejala3 * (1 - Combine_CF1_CF2)
                val Combine_CFold_CF4 = Combine_CFold_CF3 + hasilHitunganGejala4 * (1 - Combine_CFold_CF3)
                val hasilHitungGejalaCuacaA = (Combine_CFold_CF4 * 100).toString()
                PrediksiCuaca += "\nHujan Deras\n$hasilHitungGejalaCuacaA %"
            }

            // Output Semua hasil
            tvOutput!!.text = "" + PrediksiCuaca
        }
    }
}
